import { urlify } from './urlify';

describe('urlify', () => {
  it(`should replace spaces with '%20'`, () => {
    expect(urlify('Mr John Smith')).toBe('Mr%20John%20Smith');
  });
});
