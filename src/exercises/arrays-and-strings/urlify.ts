/**
 * Write a method to replace all spaces in a string with '%20'
 */
export function urlify(text: string) {
  let urlifiedText = '';
  for (let i = 0; i < text.length; i++) {
    urlifiedText += text[i] === ' ' ? '%20' : text[i];
  }
  return urlifiedText;
}
