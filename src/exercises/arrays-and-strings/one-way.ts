/**
 * Given two strings, write a function to check if they are one edit
 * (or zero edits) away.
 * 
 * @param text1 first string.
 * @param text2 second string.
 * @returns true if both strings are at most one edit away.
 */
export function oneWay(text1: string, text2: string): boolean {
  if (Math.abs(text1.length - text2.length) > 1) return false;

  const firstStrIsLargest = text1.length >= text2.length;
  let largestStr = firstStrIsLargest ? text1 : text2;
  let shortestStr = firstStrIsLargest ? text2 : text1;

  let diffCount = 0;
  const shortestChars: { [key: string]: number } = {};

  for (let char of shortestStr) {
    if (shortestChars[char]) {
      shortestChars[char]++;
    } else {
      shortestChars[char] = 1;
    }
  }

  for (let char of largestStr) {
    if (shortestChars[char] > 0) {
      shortestChars[char]--;
    } else if (diffCount + 1 > 1) {
      return false;
    } else {
      diffCount++;
    }
  }

  return true;
}
