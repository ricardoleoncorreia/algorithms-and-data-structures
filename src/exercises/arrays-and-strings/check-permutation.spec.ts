import { checkPermutation } from './check-permutation';

describe('checkPermutation', () => {
  it(`should return true if texts are 'abcd' and 'dcab'`, () => {
    expect(checkPermutation('abcd', 'dcab')).toBe(true);
  });

  it(`should return false if texts are 'abcd' and 'dccb'`, () => {
    expect(checkPermutation('abcd', 'dccb')).toBe(false);
  });
});
