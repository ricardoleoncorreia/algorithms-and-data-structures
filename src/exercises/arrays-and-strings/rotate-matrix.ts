/**
 * Given an image represented by an N x N matrix, where each pixel in the image
 * is represented by an integer, write a method to rotate the image by 90 degrees.
 * 
 * @param image image as a N x N matrix.
 * @returns 90 deg rotated matrix.
 */
export function rotateMatrix(image: number[][]): number[][] {
  const size = image.length;
  const halfMatrix = Math.floor(size / 2);
  const transpolateElements = (matrix: number[][], [x, y]: readonly [number, number]) => {
    let next = matrix[x][y];
    for (let k = 0; k < 4; k++) {
      const currentValue = next;
      [x, y] = [y, size - 1 - x];
      next = matrix[x][y];
      matrix[x][y] = currentValue;
    }
  };

  for (let i = 0; i <= halfMatrix; i++) {
    for (let j = i; j <= size - 2 - i; j++) {
      transpolateElements(image, [i, j]);
    }
  }

  return image;
}
