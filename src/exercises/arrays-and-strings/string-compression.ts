/**
 * Implement a method to perform basic string compression using the 
 * counts of repeated characters. If the "compressed" string would
 * not become smaller than the original string, your method should
 * return the original string. Assume the string has only uppercase
 * and lowercase letters (a-z).
 */
export function stringCompression(text: string): string {
  let currentChar = text[0];
  let charCount = 0;
  let compressedString = '';

  for (let char of text) {
    if (currentChar === char) {
      charCount++;
    } else {
      compressedString += `${currentChar}${charCount}`;
      currentChar = char;
      charCount = 1;
    }
  }

  compressedString += `${currentChar}${charCount}`;
  return compressedString.length < text.length ? compressedString : text;
}
