import { isUnique } from './is-unique';

describe('isUnique', () => {
  it(`should return true with 'abcdef'`, () => {
    expect(isUnique('abcdef')).toBe(true);
  });

  it(`should return false with 'abcdde'`, () => {
    expect(isUnique('abcdde')).toBe(false);
  });
});
