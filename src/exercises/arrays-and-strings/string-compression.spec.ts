import { stringCompression } from './string-compression';

describe('stringCompression', () => {
  it(`should return 'a2b1c5a3' if when text is 'aabcccccaaa'`, () => {
    expect(stringCompression('aabcccccaaa')).toBe('a2b1c5a3');
  });

  it(`should return 'abcdef' if when text is 'abcdef'`, () => {
    expect(stringCompression('abcdef')).toBe('abcdef');
  });

  it(`should return 'aabb' if when text is 'aabb'`, () => {
    expect(stringCompression('aabb')).toBe('aabb');
  });
});
