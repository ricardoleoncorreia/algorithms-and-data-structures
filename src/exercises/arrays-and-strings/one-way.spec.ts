import { oneWay } from './one-way';

describe('oneWay', () => {
  it(`should return true when original is 'pale' and edited is 'ple'`, () => {
    expect(oneWay('pale', 'ple')).toBe(true);
  });

  it(`should return true when original is 'pales' and edited is 'pale'`, () => {
    expect(oneWay('pales', 'pale')).toBe(true);
  });

  it(`should return true when original is 'pale' and edited is 'pales'`, () => {
    expect(oneWay('pale', 'pales')).toBe(true);
  });

  it(`should return true when original is 'pale' and edited is 'bale'`, () => {
    expect(oneWay('pale', 'bale')).toBe(true);
  });

  it(`should return false when original is 'pale' and edited is 'bake'`, () => {
    expect(oneWay('pale', 'bake')).toBe(false);
  });

  it(`should return false when original is 'pale' and edited is 'bakery'`, () => {
    expect(oneWay('pale', 'bakery')).toBe(false);
  });

  it(`should return true when original is 'aabb' and edited is 'aabbb'`, () => {
    expect(oneWay('aabb', 'aabbb')).toBe(true);
  });

  it(`should return false when original is 'abb' and edited is 'abbbb'`, () => {
    expect(oneWay('abb', 'abbbb')).toBe(false);
  });
});
