/**
 * Implement an algorithm to determine if a string has all
 * unique characters.
 */
export function isUnique(text: string): boolean {
  const uniqueChars: { [key: string]: string } = {};

  for (const char of text) {
    if (uniqueChars[char]) return false;
    uniqueChars[char] = char;
  }

  return true;
}
