/**
 * Given two strings, write a method to decide if one
 * is a permutation of the other.
 */
export function checkPermutation(text1: string, text2: string): boolean {
  const charOcurrences: { [key: string]: number } = {};

  for (const char of text1) {
    if (charOcurrences[char]) {
      charOcurrences[char]++;
    } else {
      charOcurrences[char] = 1;
    }
  }

  for (const char of text2) {
    if (charOcurrences[char]) {
      charOcurrences[char]--;
    } else {
      return false;
    }
  }

  return true;
}
