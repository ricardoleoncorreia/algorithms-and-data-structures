/**
 * Assume you have a method `isSubstring` which checks if one word is a substring
 * of another. Given two strings, write code to check this by using only one call
 * to `isSubstring`.
 * @param text1 first string.
 * @param text2 second string.
 * @returns true if one string is a rotation of the other one.
 */
export function stringRotation(text1: string, text2: string): boolean {
  const isSubstring = (str: string, sub: string): boolean => str.includes(sub);
  return isSubstring(text1 + text1, text2);
}
