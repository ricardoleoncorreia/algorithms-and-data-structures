/**
 * Given a string, write a function to check if it is a permutation of a palindrome.
 * Ignore casing and non-letter characters.
 */
export function palindromePermutation(text: string): boolean {
  text = text.toLowerCase();

  const lettersCount: { [key: string]: number } = {};
  for (let letter of text) {
    const charCode = letter.charCodeAt(0);
    if (charCode < 97 || charCode > 122) continue;

    if (lettersCount[letter]) lettersCount[letter]++
    else lettersCount[letter] = 1;
  }

  let oddLetters = 0;
  Object.values(lettersCount)
    .filter(count => count % 2)
    .forEach(() => oddLetters++);

  return oddLetters <= 1;
}
