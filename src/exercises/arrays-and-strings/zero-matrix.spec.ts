import { zeroMatrix } from './zero-matrix';

describe('zeroMatrix', () => {
  describe('when the matrix contains at least one zero', () => {
    it('should return a matrix with rows and columns as zero', () => {
      const original = [
        [0, 2, 3, 4, 5],
        [6, 7, 8, 9, 10],
        [11, 12, 0, 0, 15],
        [16, 17, 18, 0, 20],
        [21, 22, 23, 24, 25],
      ];
      const processed = [
        [0, 0, 0, 0, 0],
        [0, 7, 0, 0, 10],
        [0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0],
        [0, 22, 0, 0, 25],
      ];
      expect(zeroMatrix(original)).toEqual(processed);
    });
  });

  describe('when the matrix contains no zero elements', () => {
    it('should return the same matrix', () => {
      const original = [
        [1, 2, 3, 4, 5],
        [6, 7, 8, 9, 10],
        [11, 12, 13, 14, 15],
        [16, 17, 18, 19, 20],
        [21, 22, 23, 24, 25],
      ];
      expect(zeroMatrix(original)).toEqual(original);
    });
  });
});
