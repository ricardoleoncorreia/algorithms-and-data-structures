/**
 * Write an algorithm such that if an element in a M x N matrix is 0, its entire
 * row and column are set to 0.
 * 
 * @param matrix M x N matrix.
 * @returns matrix with rows and columns set to zero.
 */
export function zeroMatrix(matrix: number[][]): number[][] {
  const columnsToConvert = new Set<number>();
  const rowsToConvert = new Set<number>();

  for (let rowIndex = 0; rowIndex < matrix.length; rowIndex++) {
    const row = matrix[rowIndex];

    for (let colIndex = 0; colIndex < row.length; colIndex++) {
      if (row[colIndex]) continue;

      columnsToConvert.add(colIndex);
      rowsToConvert.add(rowIndex);
    }
  }

  columnsToConvert.forEach(colIndex => {
    for (let rowIndex = 0; rowIndex < matrix.length; rowIndex++) {
      matrix[rowIndex][colIndex] = 0;
    }
  });

  rowsToConvert.forEach(rowIndex => {
    for (let colIndex = 0; colIndex < matrix.length; colIndex++) {
      matrix[rowIndex][colIndex] = 0;
    }
  });

  return matrix;
}
