import { palindromePermutation } from './palindrome-permutation';

describe('palindromePermutation', () => {
  it(`should return false if text is 'abc ddcb'`, () => {
    expect(palindromePermutation('abc ddcb')).toBe(true);
  });

  it(`should return true if text is 'ac dacm'`, () => {
    expect(palindromePermutation('ac dacm')).toBe(false);
  });
});
