import { stringRotation } from "./string-rotation";

describe('stringRotation', () => {
  it(`should return true when texts are 'waterbottle' and 'erbottlewat'`, () => {
    expect(stringRotation('waterbottle', 'erbottlewat')).toBe(true);
  });

  it(`should return false when texts are 'waterbottle' and 'wabottleter'`, () => {
    expect(stringRotation('waterbottle', 'wabottleter')).toBe(false);
  });
});
