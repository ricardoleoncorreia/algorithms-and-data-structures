class Node {
  private _next: Node | undefined;
  get next(): Node | undefined {
    return this._next;
  }

  private _value: number;
  get value(): number {
    return this._value;
  }

  constructor(value: number) {
    this._value = value;
  }

  addNext(node: Node): void {
    if (!this._next) {
      this._next = node;
      return;
    }
    this._next.addNext(node);
  }

  setValue(value: number): void {
    this._value = value;
  }

  get(index: number): Node | undefined {
    if (index === 0) {
      return this;
    } else {
      return this._next ? this._next.get(--index) : undefined;
    }
  }

  insert(value: number, position: number): void {
    if (position === 1) {
      const newNode = new Node(value);
      this._next && newNode.addNext(this._next);
      this._next = newNode;
      return;
    }
    this._next?.insert(value, --position);
  }

  remove(index: number): void {
    if (index === 1) {
      this._next = this._next?._next;
      return;
    }
    this._next?.remove(--index);
  }

  clearNext(): void {
    this._next = undefined;
  }

  toArray(): number[] {
    return this._next ? [this._value, ...this._next.toArray()] : [this._value];
  }

  removeLastAndReturnPenultimate(): Node | undefined {
    if (this._next && !this._next._next) {
      this._next = undefined;
      return this;
    }
    return this._next && this._next.removeLastAndReturnPenultimate()
  }
}

export class SingleLinkedList {
  private _head: Node | undefined;
  get head(): Node | undefined {
    return this._head;
  }

  private _tail: Node | undefined;
  get tail(): Node | undefined {
    return this._tail;
  }

  private _length = 0;
  get length(): number {
    return this._length;
  }

  push(value: number): void {
    const newNode = new Node(value);

    if (!this._head) {
      this._head = newNode;
    } else {
      this._head.addNext(newNode);
    }

    this._tail = newNode;
    this._length++;
  }

  pop(): number | undefined {
    if (!this._tail || !this._head) return undefined;

    this._length--;
    const lastValue = this._tail.value;
    if (this._length === 1) {
      this._head = undefined;
      this._tail = undefined;
    } else {
      this._tail = this._head.removeLastAndReturnPenultimate();
    }
    return lastValue;
  }

  shift(): number | undefined {
    if (!this._head) return undefined;

    const shiftedNode = this._head;
    this._head = shiftedNode.next;
    this._length--;
    return shiftedNode.value;
  }

  unshift(value: number): void {
    const newHead = new Node(value);
    if (!this._head) {
      this._head = newHead;
      this._tail = this._head;
    } else {
      const currentHead = this._head;
      newHead.addNext(currentHead);
      this._head = newHead;
    }
    this._length++;
  }

  get(index: number): number | undefined {
    if (index < 0 || index >= this.length || !this._head || !this._tail) {
      return undefined;
    } else if (index === 0) {
      return this._head.value;
    } else if (index === this._length - 1) {
      return this._tail.value;
    }

    const nodeMatch = this._head.get(index);
    return nodeMatch ? nodeMatch.value : undefined;
  }

  set(value: number, position: number): void {
    if (!this._head) return;

    const node = this._head.get(position);
    if (!node) return;

    node.setValue(value);
  }

  insert(value: number, index: number): void {
    if (!this._head || (this._head && (index > this._length || index < 0))) {
      return;
    }

    if (index === this._length) {
      this.push(value);
      return;
    } else if (index === 0) {
      const oldHead = this._head;
      this._head = new Node(value);
      this._head.addNext(oldHead);
    } else {
      this._head.insert(value, index);
    }
    this._length++;
  }

  remove(index: number): void {
    if (!this._head || (this._head && (index >= this._length || index < 0))) {
      return;
    }

    if (index === 0 && !this._head.next) {
      this._head = undefined;
      this._tail = undefined;
    } else if (index === 0 && this._head.next) {
      this._head = this._head.next;
    } else if (index === this._length - 1) {
      this.pop()
      return;
    } else {
      this._head.remove(index);
    }

    this._length--;
  }

  // TODO: requires optimization
  reverse(): void {
    if (!this._head || !this._tail) return;

    let current = this._head;
    this._head = this._tail;
    this._tail = new Node(current.value);

    while (current.next) {
      const value = current.removeLastAndReturnPenultimate();
      this._head.addNext(value!);
    }
  }

  toArray(): number[] {
    return this._head ? this._head.toArray() : [];
  }
}
