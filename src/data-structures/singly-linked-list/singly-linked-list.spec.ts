import { SingleLinkedList } from './singly-linked-list';

describe('SingleLinkedList', () => {
  let singleLinkedList: SingleLinkedList;

  function checkSingleLinkedListState(equivalentArray: number[]) {
    const listLength = equivalentArray.length;
    const firstElementValue = equivalentArray[0];
    const lastElementValue = equivalentArray[listLength - 1];

    it(`should have ${listLength} elements`, () => {
      expect(singleLinkedList.toArray()).toEqual(equivalentArray);
    });

    it(`should have a head with value ${firstElementValue}`, () => {
      expect(singleLinkedList.head?.value).toBe(firstElementValue);
    });

    it(`should have a tail with value ${lastElementValue}`, () => {
      expect(singleLinkedList.tail?.value).toBe(lastElementValue);
    });

    it(`should have length of ${listLength}`, () => {
      expect(singleLinkedList.length).toBe(listLength);
    });
  }

  beforeEach(() => {
    singleLinkedList = new SingleLinkedList();
  });

  describe('when creating a new list', () => {
    it('should not have a head', () => {
      expect(singleLinkedList.head).toBeUndefined();
    });

    it('should not have a tail', () => {
      expect(singleLinkedList.tail).toBeUndefined();
    });

    it('should have length zero', () => {
      expect(singleLinkedList.length).toBe(0);
    });

    it('should return undefined when pop list', () => {
      expect(singleLinkedList.pop()).toBeUndefined();
    });

    it('should return undefined when shift list', () => {
      expect(singleLinkedList.shift()).toBeUndefined();
    });

    it('should have an empty array representation', () => {
      expect(singleLinkedList.toArray()).toEqual([]);
    });
  });

  describe('when adding an element with values 1, 2, 3 and 4', () => {
    beforeEach(() => {
      singleLinkedList.push(1);
      singleLinkedList.push(2);
      singleLinkedList.push(3);
      singleLinkedList.push(4);
    });

    checkSingleLinkedListState([1, 2, 3, 4]);

    describe('and pop list', () => {
      let poppedValue: number | undefined;

      beforeEach(() => {
        poppedValue = singleLinkedList.pop();
      });

      it('should return the popped value', () => {
        expect(poppedValue).toBe(4);
      });

      checkSingleLinkedListState([1, 2, 3]);
    });

    describe('and shift list', () => {
      let shiftedValue: number | undefined;

      beforeEach(() => {
        shiftedValue = singleLinkedList.shift();
      });

      it('should return the shifted value', () => {
        expect(shiftedValue).toBe(1);
      });

      checkSingleLinkedListState([2, 3, 4]);
    });

    describe('and unshift value 5', () => {
      beforeEach(() => {
        singleLinkedList.unshift(5);
      });

      checkSingleLinkedListState([5, 1, 2, 3, 4]);
    });

    describe('and get value at first position', () => {
      it('should retrieve value 1', () => {
        expect(singleLinkedList.get(0)).toBe(1);
      });
    });

    describe('and get value at a middle position', () => {
      it('should retrieve value 2', () => {
        expect(singleLinkedList.get(1)).toBe(2);
      });
    });

    describe('and get value at last position', () => {
      it('should retrieve value 4', () => {
        const lastIndex = singleLinkedList.length - 1;
        expect(singleLinkedList.get(lastIndex)).toBe(4);
      });
    });

    describe('and getting a value at outbound position', () => {
      it('should retrieve undefined', () => {
        const outboundIndex = singleLinkedList.length + 1;
        expect(singleLinkedList.get(outboundIndex)).toBeUndefined();
      });
    });

    describe('and set value 6 at first position', () => {
      beforeEach(() => {
        singleLinkedList.set(6, 0);
      });

      checkSingleLinkedListState([6, 2, 3, 4]);
    });

    describe('and set value 6 at a middle position', () => {
      beforeEach(() => {
        singleLinkedList.set(6, 1);
      });

      checkSingleLinkedListState([1, 6, 3, 4]);
    });

    describe('and set value 6 at last position', () => {
      beforeEach(() => {
        const lastIndex = singleLinkedList.length - 1;
        singleLinkedList.set(6, lastIndex);
      });

      checkSingleLinkedListState([1, 2, 3, 6]);
    });

    describe('and setting a value at outbound position', () => {
      beforeEach(() => {
        const outboundIndex = singleLinkedList.length + 1;
        singleLinkedList.set(6, outboundIndex);
      });

      checkSingleLinkedListState([1, 2, 3, 4]);
    });

    describe('and insert value 7 at first position', () => {
      beforeEach(() => {
        singleLinkedList.insert(7, 0);
      });

      checkSingleLinkedListState([7, 1, 2, 3, 4]);
    });

    describe('and insert value 7 at a middle position', () => {
      beforeEach(() => {
        singleLinkedList.insert(7, 1);
      });

      checkSingleLinkedListState([1, 7, 2, 3, 4]);
    });

    describe('and insert value 7 at last position', () => {
      beforeEach(() => {
        const lastIndex = singleLinkedList.length - 1;
        singleLinkedList.insert(7, lastIndex);
      });

      checkSingleLinkedListState([1, 2, 3, 7, 4]);
    });

    describe('and inserting a value at position next to last element', () => {
      beforeEach(() => {
        const nextToLastIndex = singleLinkedList.length;
        singleLinkedList.insert(7, nextToLastIndex);
      });

      checkSingleLinkedListState([1, 2, 3, 4, 7]);
    });

    describe('and inserting a value at outbound position', () => {
      beforeEach(() => {
        const outboundIndex = singleLinkedList.length + 1;
        singleLinkedList.insert(7, outboundIndex);
      });

      checkSingleLinkedListState([1, 2, 3, 4]);
    });

    describe('and removing value at first position', () => {
      beforeEach(() => {
        singleLinkedList.remove(0);
      });

      checkSingleLinkedListState([2, 3, 4]);
    });

    describe('and removing value at a middle position', () => {
      beforeEach(() => {
        singleLinkedList.remove(1);
      });

      checkSingleLinkedListState([1, 3, 4]);
    });

    describe('and removing value at last position', () => {
      beforeEach(() => {
        const lastIndex = singleLinkedList.length - 1;
        singleLinkedList.remove(lastIndex);
      });

      checkSingleLinkedListState([1, 2, 3]);
    });

    describe('and removing a value at outbound position', () => {
      beforeEach(() => {
        const outboundIndex = singleLinkedList.length + 1;
        singleLinkedList.remove(outboundIndex);
      });

      checkSingleLinkedListState([1, 2, 3, 4]);
    });

    describe('and reversing the list', () => {
      beforeEach(() => {
        singleLinkedList.reverse();
      });

      checkSingleLinkedListState([4, 3, 2, 1]);
    });
  });
});
