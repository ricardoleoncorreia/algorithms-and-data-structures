/** @type {import('ts-jest/dist/types').InitialOptionsTsJest} */
module.exports = {
  collectCoverageFrom: [
    "src/**/*.ts",
  ],
  preset: 'ts-jest',
  testEnvironment: 'node',
};
